<!DOCTYPE HTML>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,JavaScript">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Goopter Project Two</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- fontawesome.min.css -->
    <script src="https://kit.fontawesome.com/012c438b84.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/fontawesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    
    <!-- style.css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- responsive.css -->
    <link rel="stylesheet" href="css/responsive.css">


</head>


<body>



<section class="header-section">
    <div class="container header-con ">
        <div class="row header-row d-flex flex-row" id="wrapper"> 

                <!-- Sidebar -->
                <div class="bg-light border-right" id="sidebar-wrapper">
                  <div class="list-group list-group-flush">
                    <a href="#" class="list-group-item list-group-item-action bg-light">Home</a>
                    <a href="#" class="list-group-item list-group-item-action bg-light">About Us</a>
                    <a href="#" class="list-group-item list-group-item-action bg-light">Medical Services</a>
                    <a href="#" class="list-group-item list-group-item-action bg-light">Departments</a>
                    <a href="#" class="list-group-item list-group-item-action bg-light">Timetable</a>
                    <a href="#" class="list-group-item list-group-item-action bg-light">Contact</a>
                  </div>
                </div>
                <!-- /#sidebar-wrapper -->


                <div class="menu-toggle-div">
                     <i class="fas fa-bars" id="menu-toggle"></i>
                </div>


            <div id="header-logo-div">
                <a href="#" class="logo">
                    <img src="images/logo_new(1).webp" class="img-fluid" alt="Responsive image">
                </a>    
            </div>

            <div class="">
                <div class="d-flex justify-content-end">

                    <nav class="navbar navbar-expand-lg navbar-light nav-row ">

                        <div class="collapse navbar-collapse nav-div" id="navbarSupportedContent">
                            <ul class="navbar-nav d-flex justify-content-end">
                                <li class="nav-item dropdown active">
                                    <a class="nav-link active-class" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       Home <span class="sr-only">(current)</span>
                                    </a>
                                    <div class="dropdown-menu nav-dropdown" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Booking Form</a>
                                        <a class="dropdown-item" href="#">Service Boxes</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Medical Services</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Departments</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Timetable</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="about-section mt-3">
    <div class="container">
        <div class="row about-row d-flex flex-row justify-content-between">
            <div class="about-us-section">
                <h3 class="text-divider-double">About Us</h3>
                <div class="sep"></div>
            </div>

            <div class="meet-doctor-section">
                <h3 class="text-divider-double">Meet Our Doctors</h3>
                <div class="sep"></div>
            </div>
        </div>
    </div>
</section>